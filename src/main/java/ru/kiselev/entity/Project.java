package ru.kiselev.entity;

import lombok.Data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class Project {
    private long id;
    private long idUser;
    private String name;
    private String description;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private List<Long> taskIdList = new ArrayList<>();

    @Override
    public String toString() {
        return "name = " + name + " (ID: " + id + ")";
    }

    public void addNewTask(Long id) {
        taskIdList.add(id);
    }
}
