package ru.kiselev.entity;

import lombok.Data;

import java.time.LocalDate;

@Data
public class Task {
    private long id;
    private long idUser;
    private long idPrj;
    private String name;
    private String description;
    private LocalDate dateFrom;
    private LocalDate dateTo;


    @Override
    public String toString() {
        return "name = " + name + " (ID: " + id + ")";
    }
}
