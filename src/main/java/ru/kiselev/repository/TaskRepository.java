package ru.kiselev.repository;

import ru.kiselev.entity.Task;
import ru.kiselev.exception.TaskNotFoundException;

import java.util.*;

public class TaskRepository implements Repository<Long, Task> {
    private final Map<Long, Task> taskMap = new HashMap<>();
    private long countId = 0;

    @Override
    public Task create(Task task) {
        long nextId = ++countId;
        task.setId(nextId);
        return taskMap.put(nextId, task);
    }

    @Override
    public void clear() {
        taskMap.clear();
    }

    @Override
    public Task remove(Long aLong) {
        return taskMap.remove(aLong);
    }

    @Override
    public List<Task> readAll() {
        return new ArrayList<>(taskMap.values());
    }

    @Override
    public List<Task> readAll(Set<Long> id) {
        List<Task> taskList = new ArrayList<>();
        for (long idTask : id) {
            Task task = taskMap.get(idTask);
            taskList.add(task);
        }
        return taskList;
    }

    @Override
    public Task read(Long aLong) {
        Task task = taskMap.get(aLong);
        if (task == null) {
            throw new TaskNotFoundException(aLong);
        }
        return task;
    }
}
