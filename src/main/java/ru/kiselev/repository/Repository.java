package ru.kiselev.repository;

import java.util.List;
import java.util.Set;

public interface Repository<ID, ENTITY> {

    ENTITY create(ENTITY entity);

    void clear();

    ENTITY remove(ID id);

    List<ENTITY> readAll();

    List<ENTITY> readAll(Set<ID> id);

    ENTITY read(ID id);

}
