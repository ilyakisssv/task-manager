package ru.kiselev.repository;

import ru.kiselev.exception.UserNotFoundException;
import ru.kiselev.users.User;

import java.util.*;

public class UserRepository implements Repository<Long, User> {
    private final Map<Long, User> userMap = new HashMap<>();
    private long count = 0;

    @Override
    public User create(User user) {
        long id = ++count;
        user.setIdUser(id);
        return userMap.put(id, user);
    }

    @Override
    public void clear() {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public User remove(Long aLong) {
        return userMap.remove(aLong);
    }

    @Override
    public List<User> readAll() {
        return new ArrayList<>(userMap.values());
    }

    @Override
    public List<User> readAll(Set<Long> id) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public User read(Long aLong) {
        User user = userMap.get(aLong);
        if (user == null) {
            throw new UserNotFoundException(aLong);
        }
        return user;
    }
}
