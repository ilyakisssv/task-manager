package ru.kiselev.repository;

import ru.kiselev.entity.Project;
import ru.kiselev.exception.ProjectNotFoundException;

import java.util.*;

public class ProjectRepository implements Repository<Long, Project> {
    private final Map<Long, Project> projectMap = new HashMap<>();
    private long countId = 0;

    @Override
    public Project create(Project project) {
        long nextId = ++countId;
        project.setId(nextId);
        return projectMap.put(nextId, project);
    }

    @Override
    public void clear() {
        projectMap.clear();
    }

    @Override
    public Project remove(Long aLong) {
        return projectMap.remove(aLong);
    }

    @Override
    public List<Project> readAll() {
        return new ArrayList<>(projectMap.values());
    }

    @Override
    public List<Project> readAll(Set<Long> id) {
        List<Project> projectList = new ArrayList<>();
        for (long idProject : id) {
            Project project = projectMap.get(idProject);
            projectList.add(project);
        }
        return projectList;
    }

    @Override
    public Project read(Long aLong) {
        Project project = projectMap.get(aLong);
        if (project == null) {
            throw new ProjectNotFoundException(aLong);
        }
        return project;
    }
}
