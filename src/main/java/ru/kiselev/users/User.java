package ru.kiselev.users;

import lombok.Data;

@Data
public class User {
    private long idUser;
    private String login;
    private String password;
    private Role role;
}
