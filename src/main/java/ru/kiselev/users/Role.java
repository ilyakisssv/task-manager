package ru.kiselev.users;

public enum Role {
    USER("Пользователь"),
    ADMIN("Админисратор");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }
}
