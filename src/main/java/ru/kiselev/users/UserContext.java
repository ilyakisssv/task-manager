package ru.kiselev.users;

import jakarta.xml.bind.DatatypeConverter;
import ru.kiselev.exception.UserNotAuthorizationException;
import ru.kiselev.exception.UserNotFoundException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class UserContext {
    private static User userSave;

    public static long getIdUser() {
        if (userSave == null) {
            throw new UserNotFoundException();
        }
        return userSave.getIdUser();
    }

    public static void cleanContext() {
        userSave = null;
    }

    public static void checkAuthorizationUser() {
        if (userSave != null) {
            return;
        }
        throw new UserNotAuthorizationException();
    }

    public static User getUser() {
        return userSave;
    }

    public static void setUser(User user) {
        userSave = user;
    }

    public static String hashPass(String pass) {
        try {
            MessageDigest digester = MessageDigest.getInstance("SHA-512");
            byte[] input = pass.getBytes();
            byte[] digest = digester.digest(input);
            return DatatypeConverter.printHexBinary(digest);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e);
        }
    }
}