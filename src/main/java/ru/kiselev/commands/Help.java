package ru.kiselev.commands;

import lombok.Data;

import java.util.Map;

import static ru.kiselev.service.ConstantsList.HELP;


@Data
public class Help implements CmdInterface {
    private final Map<String, CmdInterface> cmdMap;
    private final String name = HELP;
    private final String description = "Список доступных команд.";

    public Help(Map<String, CmdInterface> cmdMap) {
        this.cmdMap = cmdMap;
    }

    @Override
    public void execute() {
        System.out.println("\nВыбирите одну из предложенных команд:");
        for (CmdInterface cmd : cmdMap.values()) {
            String name = cmd.getName();
            String description = cmd.getDescription();
            String formatCmd = String.format("%s: %s", name, description);
            System.out.println(formatCmd);
        }
    }
}
