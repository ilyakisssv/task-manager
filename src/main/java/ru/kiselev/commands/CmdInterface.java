package ru.kiselev.commands;

import java.io.IOException;

public interface CmdInterface {
    String getName();

    String getDescription();

    void execute() throws IOException;
}
