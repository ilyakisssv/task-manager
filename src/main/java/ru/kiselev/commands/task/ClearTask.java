package ru.kiselev.commands.task;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.entity.Project;
import ru.kiselev.service.EditorContext;
import ru.kiselev.service.TaskService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

import static ru.kiselev.service.ConstantsList.TASK_CLEAR;


@Data
public class ClearTask implements CmdInterface {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final String name = TASK_CLEAR;
    private final String description = "Удалить все задачи проекта.";
    private final TaskService taskService;

    @Override
    public void execute() {
        Project project = EditorContext.getCurrentProject();
        if (project.getTaskIdList().size() == 0) {
            System.out.println("Список задач пуст.");
            return;
        }
        Set<Long> listId = new HashSet<>(project.getTaskIdList());
        taskService.removeTaskList(listId);
        project.getTaskIdList().clear();
        System.out.println("Все задачи проекта успешно удалены.\n");
    }
}
