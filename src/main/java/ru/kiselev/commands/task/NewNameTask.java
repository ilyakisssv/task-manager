package ru.kiselev.commands.task;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.entity.Task;
import ru.kiselev.exception.NotInputException;
import ru.kiselev.service.EditorContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static ru.kiselev.service.ConstantsList.TASK_NEW_NAME;


@Data
public class NewNameTask implements CmdInterface {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final String name = TASK_NEW_NAME;
    private final String description = "Изменить имя задачи.";

    @Override
    public void execute() throws IOException {
        Task task = EditorContext.getCurrentTask();
        System.out.println("\nВведите новое имя задачи: ");
        String name = reader.readLine();
        if (name.isEmpty()) {
            throw new NotInputException();
        }
        task.setName(name);
        System.out.println("Имя задачи успешно изменено.\n");
    }
}
