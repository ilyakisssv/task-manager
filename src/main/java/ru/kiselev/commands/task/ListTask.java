package ru.kiselev.commands.task;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.entity.Task;
import ru.kiselev.service.TaskService;
import ru.kiselev.users.UserContext;

import java.util.List;

import static ru.kiselev.service.ConstantsList.TASK_LIST;


@Data
public class ListTask implements CmdInterface {
    private final TaskService taskService;
    private final String name = TASK_LIST;
    private final String description = "Список всех задач.";

    @Override
    public void execute() {
        UserContext.checkAuthorizationUser();
        List<Task> taskList = taskService.getListTask();
        if (taskList.isEmpty()) {
            System.out.println("Список задач пуст!");
            return;
        }
        for (int i = 0; i < taskList.size(); i++) {
            Task task = taskList.get(i);
            String line = String.format("%s. %s", i + 1, task);
            System.out.println(line);
        }
    }
}
