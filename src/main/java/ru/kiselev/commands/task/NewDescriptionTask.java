package ru.kiselev.commands.task;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.entity.Task;
import ru.kiselev.exception.NotInputException;
import ru.kiselev.service.EditorContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static ru.kiselev.service.ConstantsList.TASK_NEW_DESCRIPTION;


@Data
public class NewDescriptionTask implements CmdInterface {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final String name = TASK_NEW_DESCRIPTION;
    private final String description = "Изменить описание задачи.";

    @Override
    public void execute() throws IOException {
        Task task = EditorContext.getCurrentTask();
        System.out.println("\nВведите новое описание задачи: ");
        String description = reader.readLine();
        if (description.isEmpty()) {
            throw new NotInputException();
        }
        task.setDescription(description);
        System.out.println("Описание задачи успешно изменено.\n");
    }
}

