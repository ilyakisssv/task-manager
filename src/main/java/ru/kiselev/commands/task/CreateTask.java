package ru.kiselev.commands.task;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.entity.Task;
import ru.kiselev.exception.NotInputException;
import ru.kiselev.service.TaskService;
import ru.kiselev.users.UserContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.temporal.TemporalAccessor;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static ru.kiselev.service.ConstantsList.TASK_CREATE;


@Data
public class CreateTask implements CmdInterface {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final TaskService taskService;
    private final String name = TASK_CREATE;
    private final String description = "Добавить новую задачу.";

    @Override
    public void execute() throws IOException {
        UserContext.checkAuthorizationUser();
        Task task = new Task();

        task.setIdUser(UserContext.getIdUser());
        setNameTask(task);
        setDescriptionTask(task);
        setDataFromTask(task);
        setDataToTask(task);
        taskService.createTask(task);

        System.out.println("Задача успешно добавлена. \n");
    }

    private void setDataToTask(Task task) throws IOException {
        try {
            System.out.println("Добавьте дату окончания задачи (в формате 2000-10-10): ");
            String dataTo = reader.readLine();
            TemporalAccessor accessorT = ISO_LOCAL_DATE.parse(dataTo);
            LocalDate dateT = LocalDate.from(accessorT);
            task.setDateTo(dateT);
            System.out.println("Дата окончания успешно добавлена!\n");
        } catch (DateTimeException e) {
            System.out.println("Неверный формат даты. Введите заново.\n");
            setDataToTask(task);
        }
    }

    private void setDataFromTask(Task task) throws IOException {
        try {
            System.out.println("Добавьте дату начала задачи (в формате 2000-10-10): ");
            String dataFrom = reader.readLine();
            TemporalAccessor accessorF = ISO_LOCAL_DATE.parse(dataFrom);
            LocalDate dateF = LocalDate.from(accessorF);
            task.setDateFrom(dateF);
            System.out.println("Дата начала успешно добавлена!\n");
        } catch (DateTimeException e) {
            System.out.println("Неверный формат даты. Введите заново.\n");
            setDataFromTask(task);
        }
    }

    private void setDescriptionTask(Task task) throws IOException {
        System.out.println("Добавьте описание к задаче: ");
        String description = reader.readLine();
        task.setDescription(description);
        System.out.println("Описание успешно добавлено!\n");
    }

    private void setNameTask(Task task) throws IOException {
        System.out.println("\nВведите имя задачи: ");
        String taskName = reader.readLine();
        if (taskName.isEmpty()) {
            throw new NotInputException();
        }
        task.setName(taskName);
        System.out.println("Имя успешно добавлено.\n");
    }
}
