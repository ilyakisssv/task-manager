package ru.kiselev.commands.task;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.service.TaskService;
import ru.kiselev.users.UserContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static ru.kiselev.service.ConstantsList.TASK_REMOVE;


@Data
public class RemoveTask implements CmdInterface {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final TaskService taskService;
    private final String name = TASK_REMOVE;
    private final String description = "Удалить задачу";

    @Override
    public void execute() throws IOException {
        UserContext.checkAuthorizationUser();

        System.out.println("\nВведите ID задачи которую надо удалить: ");
        long id = Long.parseLong(reader.readLine());
        taskService.removeTask(id);

        System.out.println("Задача с id: " + id + " успешно удалена. \n");
    }
}
