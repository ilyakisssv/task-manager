package ru.kiselev.commands.task;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.entity.Task;
import ru.kiselev.service.CommandTerminal;
import ru.kiselev.service.EditorContext;
import ru.kiselev.service.TaskService;
import ru.kiselev.users.UserContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static ru.kiselev.service.ConstantsList.TASK_OPEN;

@Data
public class OpenTask implements CmdInterface {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final TaskService taskService;
    private final CommandTerminal commandTerminal;
    private final String name = TASK_OPEN;
    private final String description = "открыть задачу.";

    @Override
    public void execute() throws IOException {
        UserContext.checkAuthorizationUser();
        System.out.println("\nВведите ID выбраной задачи: ");
        long idTsk = Long.parseLong(reader.readLine());
        Task task = taskService.getTask(idTsk);
        EditorContext.setCurrentTask(task);

//        Map<String, CmdInterface> cmdTask = new HashMap<String, CmdInterface>() {{
//            put(TASK_NEW_NAME, new NewNameTask(task));
//            put(TASK_NEW_DESCRIPTION, new NewDescriptionTask(task));
//            put("help", new Help(this));
//        }};
//
//        CommandHolder commandHolder = new CommandHolder(cmdTask);

        showTask(task);
        EditorContext.clean();

//        selectCommand(commandHolder);
    }
//
//    private void selectCommand(CommandHolder commandHolder) {
//        System.out.println("\nДля вызова списка команд введите: help.");
//        System.out.println("Для завершения работы с задачей введите: exit.");
//        while (true) {
//            try {
//                String input = reader.readLine();
//                if (input.equals("exit")) {
//                    break;
//                } else if (input.isEmpty()) {
//                    throw new NotInputException();
//                }
//                commandHolder.findCommand(input).execute();
//            } catch (Exception e) {
//                System.out.println("Произошла ошибка: " + e + ".\n");
//            }
//        }
//    }

    private void showTask(Task task) {
        System.out.println("\nНазвание задачи: " + task.getName());
        System.out.println("Описание задачи: " + task.getDescription());
        System.out.println("ID проекта в который вложена задача: " + task.getIdPrj());
        System.out.println("Дата начала задачи: " + task.getDateFrom());
        System.out.println("Дата окончания задачи: " + task.getDateTo());
    }
}
