package ru.kiselev.commands.user;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.service.UserService;
import ru.kiselev.users.Role;
import ru.kiselev.users.User;
import ru.kiselev.users.UserContext;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@Data
public class UserView implements CmdInterface {
    private final UserService userService;
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final String name = "user-view";
    private final String description = "Просмотр и редактирование данных пользователя.";

    public UserView(UserService userService) {
        this.userService = userService;
    }

    public void execute() {
        User user = UserContext.getUser();
        System.out.println("Данные пользователя: ");
        String login = user.getLogin();
        Role role = user.getRole();
        String line = String.format("Мой логин: %s \nМоя роль: %s", login, role);
        System.out.println(line + "\n");
    }
}
