package ru.kiselev.commands.user;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.exception.NotInputException;
import ru.kiselev.exception.NotMatchPassword;
import ru.kiselev.service.UserService;
import ru.kiselev.users.Role;
import ru.kiselev.users.User;
import ru.kiselev.users.UserContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Data
public class UserCreate implements CmdInterface {
    public final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final UserService userService;
    private final String name = "user-create";
    private final String description = "Зарегистировать нового пользователя.";

    public UserCreate(UserService userService) {
        this.userService = userService;
    }

    public void execute() throws IOException {
        System.out.println("Создать нового пользователя.");
        User user = new User();

        addLoginUser(user);
        addPasswordUser(user);
        setRoleUser(user);

        userService.createUser(user);
    }

    private void setRoleUser(User user) {
        user.setRole(Role.USER);
    }

    private void addPasswordUser(User user) throws IOException {
        System.out.println("\nСоздайте пароль для пользователя: ");
        String password = reader.readLine();
        if (password.isEmpty()) {
            throw new NotInputException();
        }
        System.out.println("Повторите ввод пароля: ");
        String checkPassword = reader.readLine();
        if (checkPassword.isEmpty()) {
            throw new NotInputException();
        }
        if (!(password.equals(checkPassword))) {
            throw new NotMatchPassword();
        }
        user.setPassword(UserContext.hashPass(password));
        System.out.println("Пароль успешно присвоен.\n");
    }

    private void addLoginUser(User user) throws IOException {
        System.out.println("\nВведте логин: ");
        String login = reader.readLine();
        if (login.isEmpty()) {
            throw new NotInputException();
        }
        userService.checkingExistingUser(login);
        user.setLogin(login);
        System.out.println("Лоигин успешно присвоен.");
    }
}
