package ru.kiselev.commands.user;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.exception.NotInputException;
import ru.kiselev.service.UserService;
import ru.kiselev.users.User;
import ru.kiselev.users.UserContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Data
public class UpdatePassword implements CmdInterface {
    private final UserService userService;
    private final String name = "update-password";
    private final String description = "Обновить пароль пользователя.";
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public UpdatePassword(UserService userService) {
        this.userService = userService;
    }

    public void execute() throws IOException {
        UserContext.checkAuthorizationUser();
        long id = UserContext.getIdUser();
        User user = userService.getUser(id);
        System.out.println("Изменить пароль пользователя.\n");
        System.out.println("Введите старый пароль: ");
        String oldPassword = reader.readLine();
        if (!(UserContext.hashPass(user.getPassword()).equals(UserContext.hashPass(oldPassword)))) {
            System.out.println("Пароли не совпали! Попробуйте снова.\n");
        }
        System.out.println("Введите новый пароль: ");
        String password = reader.readLine();
        if (password.isEmpty()) {
            throw new NotInputException();
        }
        user.setPassword(UserContext.hashPass(password));
        System.out.println("Пароль успешно изменен.\n");
    }
}
