package ru.kiselev.commands.user;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.users.UserContext;

@Data
public class EndOfSession implements CmdInterface {
    private final String name = "end-session";
    private final String description = "Завершить сессию пользователя.";

    public void execute() {
        UserContext.cleanContext();
        System.out.println("Сессия успешно завершена.\n");
    }
}
