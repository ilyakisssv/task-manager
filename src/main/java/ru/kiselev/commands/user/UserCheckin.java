package ru.kiselev.commands.user;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.exception.NotInputException;
import ru.kiselev.service.UserService;
import ru.kiselev.users.User;
import ru.kiselev.users.UserContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Data
public class UserCheckin implements CmdInterface {
    private final UserService userService;
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final String name = "user-checkin";
    private final String description = "Авториация пользователя.";

    public UserCheckin(UserService userService) {
        this.userService = userService;
    }

    public void execute() throws IOException {
        System.out.println("\nВойти в профиль.");
        System.out.println("Введите логин пользователя: ");
        String login = reader.readLine();
        if (login.isEmpty()) {
            throw new NotInputException();
        }
        User user = userService.checkLoginUser(login);
        System.out.println("Введите пароль от учетной записи: ");
        String password = reader.readLine();
        if (password.isEmpty()) {
            throw new NotInputException();
        }
        userService.checkUser(user, UserContext.hashPass(password));
    }
}
