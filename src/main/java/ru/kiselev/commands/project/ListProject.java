package ru.kiselev.commands.project;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.entity.Project;
import ru.kiselev.service.ProjectService;
import ru.kiselev.users.UserContext;

import java.util.List;

import static ru.kiselev.service.ConstantsList.PROJECT_LIST;


@Data
public class ListProject implements CmdInterface {
    private final ProjectService projectService;
    private final String name = PROJECT_LIST;
    private final String description = "Список всех проектов.";

    @Override
    public void execute() {
        UserContext.checkAuthorizationUser();
        List<Project> projectList = projectService.getListProject();
        if (projectList.isEmpty()) {
            System.out.println("Список проектов пуст. \n");
            return;
        }
        for (int i = 0; i < projectList.size(); i++) {
            Project project = projectList.get(i);
            String line = String.format("%s. %s", i + 1, project);
            System.out.println(line);
        }
    }
}
