package ru.kiselev.commands.project;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.entity.Project;
import ru.kiselev.exception.NotInputException;
import ru.kiselev.service.ProjectService;
import ru.kiselev.users.UserContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.temporal.TemporalAccessor;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static ru.kiselev.service.ConstantsList.PROJECT_CREATE;


@Data
public class CreateProject implements CmdInterface {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final ProjectService projectService;
    private final String name = PROJECT_CREATE;
    private final String description = "Добавить новый проект.";

    @Override
    public void execute() throws IOException {
        UserContext.checkAuthorizationUser();
        Project project = new Project();

        project.setIdUser(UserContext.getIdUser());
        setNameToProject(project);
        setDescriptionProject(project);
        setDataFromProject(project);
        setDataToProject(project);
        projectService.createProject(project);

        System.out.println("Проект успешно создан.\n");
    }

    private void setDataToProject(Project project) throws IOException {
        System.out.println("Добавьте дату окончания проекта (в формате 2000-10-10): ");
        try {
            String dataTo = reader.readLine();
            TemporalAccessor accessorT = ISO_LOCAL_DATE.parse(dataTo);
            LocalDate dateT = LocalDate.from(accessorT);
            project.setDateTo(dateT);
            System.out.println("Дата окончания успешно добавлена!\n");
        } catch (DateTimeException e) {
            System.out.println("Неверный формат даты. Введите заново.\n");
            setDataToProject(project);
        }
    }

    private void setDataFromProject(Project project) throws IOException {
        System.out.println("Добавьте дату начала проекта (в формате 2000-10-10): ");
        try {
            String dataFrom = reader.readLine();
            TemporalAccessor accessorF = ISO_LOCAL_DATE.parse(dataFrom);
            LocalDate dateF = LocalDate.from(accessorF);
            project.setDateFrom(dateF);
            System.out.println("Дата начала успешно добавлена!\n");
        } catch (DateTimeException e) {
            System.out.println("Неверный формат даты. Введите заново.\n");
            setDataFromProject(project);
        }
    }

    private void setDescriptionProject(Project project) throws IOException {
        System.out.println("Добавьте описание к проекту: ");
        String desc = reader.readLine();
        project.setDescription(desc);
        System.out.println("Описание успешно добавлено!\n");
    }

    private void setNameToProject(Project project) throws IOException {
        System.out.println("Введите имя проекта: ");
        String namePrj = reader.readLine();
        if (namePrj.isEmpty()) {
            throw new NotInputException();
        }
        project.setName(namePrj);
        System.out.println("Имя успешно добавлено.\n");
    }
}

