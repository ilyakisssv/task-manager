package ru.kiselev.commands.project;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.entity.Project;
import ru.kiselev.exception.NotInputException;
import ru.kiselev.service.EditorContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static ru.kiselev.service.ConstantsList.PROJECT_NEW_DESCRIPTION;


@Data
public class NewDescriptionProject implements CmdInterface {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final String name = PROJECT_NEW_DESCRIPTION;
    private final String description = "Изменить описание проекта.";

    @Override
    public void execute() throws IOException {
        Project project = EditorContext.getCurrentProject();
        System.out.println("\nВведите новое описание проекта: ");
        String description = reader.readLine();
        if (description.isEmpty()) {
            throw new NotInputException();
        }
        project.setDescription(description);
        System.out.println("Описание проекта успешно изменено.\n");
    }
}

