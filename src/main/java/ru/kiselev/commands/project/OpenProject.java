package ru.kiselev.commands.project;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.entity.Project;
import ru.kiselev.entity.Task;
import ru.kiselev.service.CommandTerminal;
import ru.kiselev.service.EditorContext;
import ru.kiselev.service.ProjectService;
import ru.kiselev.service.TaskService;
import ru.kiselev.users.UserContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import static ru.kiselev.service.ConstantsList.PROJECT_OPEN;

@Data
public class OpenProject implements CmdInterface {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final ProjectService projectService;
    private final TaskService taskService;
    private final CommandTerminal commandTerminal;
    private final String name = PROJECT_OPEN;
    private final String description = "Открыть проект.";

    @Override
    public void execute() throws IOException {
        UserContext.checkAuthorizationUser();

        System.out.println("\nВведите ID выбраного проекта");
        long idPrj = Long.parseLong(reader.readLine());
        Project project = projectService.getProject(idPrj);
        EditorContext.setCurrentProject(project);

//        Map<String, CmdInterface> cmdProject = new HashMap<String, CmdInterface>() {{
//            put(PROJECT_NEW_NAME, new NewNameProject(project));
//            put(PROJECT_NEW_DESCRIPTION, new NewDescriptionProject(project));
//            put(PROJECT_ADD_TASK, new ProjectAddTask(taskService, project));
//            put(PROJECT_CLEAR_TASK, new ProjectClearTask(project, taskService));
//            put(TASK_CLEAR, new ClearTask(project, taskService));
//            put("help", new Help(this));
//        }};
//
//        CommandHolder commandHolder = new CommandHolder(cmdProject);

        showProject(project);
        commandTerminal.startCommand();
        EditorContext.clean();
    }

//    private void selectCommand(CommandHolder commandHolder) {
//        System.out.println("\nДля вызова списка команд введите: help.");
//        System.out.println("Для завершения работы с проектом введите: exit.");
//        while (true) {
//            try {
//                String input = reader.readLine();
//                if (input.equals("exit")) {
//                    break;
//                } else if (input.isEmpty()) {
//                    throw new NotInputException();
//                }
//                commandHolder.findCommand(input).execute();
//            } catch (Exception e) {
//                System.out.println("Ошибка: " + e);
//            }
//        }
//    }

    private void showProjectTasks(Project project) {
        if (project.getTaskIdList().isEmpty()) {
            System.out.println("Список задач пуст.");
            return;
        }
        List<Long> idTask = project.getTaskIdList();
        for (Long id : idTask) {
            Task task = taskService.getTask(id);
            long num = project.getTaskIdList().indexOf(id) + 1;
            String formatLine = String.format("%s. %s", num, task);
            System.out.println(formatLine);
        }
    }

    private void showProject(Project project) {
        System.out.println("Имя проекта: " + project.getName());
        System.out.println("Описание проекта: " + project.getDescription());
        System.out.println("Дата начала проекта: " + project.getDateFrom());
        System.out.println("Дата окончания проекта: " + project.getDateTo());
        System.out.println("\nЗадачи проекта: ");
        showProjectTasks(project);
    }
}

