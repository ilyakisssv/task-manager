package ru.kiselev.commands.project;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.entity.Project;
import ru.kiselev.service.ProjectService;
import ru.kiselev.service.TaskService;
import ru.kiselev.users.UserContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

import static ru.kiselev.service.ConstantsList.PROJECT_REMOVE;


@Data
public class RemoveProject implements CmdInterface {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final TaskService taskService;
    private final ProjectService projectService;
    private final String name = PROJECT_REMOVE;
    private final String description = "Удалить проект.";

    @Override
    public void execute() throws IOException {
        UserContext.checkAuthorizationUser();

        System.out.println("Введите ID проекта который надо удалить: ");
        long id = Long.parseLong(reader.readLine());
        Project project = projectService.getProject(id);

        if (project.getTaskIdList().size() > 0) {
            Set<Long> listId = new HashSet<>(project.getTaskIdList());
            taskService.removeTaskList(listId);
        }
        projectService.removeProject(id);
        System.out.println("Проект с id: " + id + " успешно удален.\n");
    }
}
