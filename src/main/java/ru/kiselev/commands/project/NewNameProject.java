package ru.kiselev.commands.project;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.entity.Project;
import ru.kiselev.exception.NotInputException;
import ru.kiselev.service.EditorContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static ru.kiselev.service.ConstantsList.PROJECT_NEW_NAME;


@Data
public class NewNameProject implements CmdInterface {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final String name = PROJECT_NEW_NAME;
    private final String description = "Изменить имя проекта.";

    @Override
    public void execute() throws IOException {
        System.out.println("\nВведите новое имя проекта: ");
        String name = reader.readLine();
        if (name.isEmpty()) {
            throw new NotInputException();
        }
        Project project = EditorContext.getCurrentProject();
        project.setName(name);
        System.out.println("Имя проекта успешно изменено.\n");
    }
}
