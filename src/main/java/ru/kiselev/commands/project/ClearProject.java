package ru.kiselev.commands.project;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.service.ProjectService;
import ru.kiselev.users.UserContext;

import static ru.kiselev.service.ConstantsList.PROJECT_CLEAR;


@Data
public class ClearProject implements CmdInterface {
    private final ProjectService projectService;
    private final String name = PROJECT_CLEAR;
    private final String description = "Удалить все проекты.";

    @Override
    public void execute() {
        UserContext.checkAuthorizationUser();
        projectService.clearProject();
        System.out.println("Все проекты успешно удалены. \n");
    }
}
