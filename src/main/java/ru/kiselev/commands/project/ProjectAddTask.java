package ru.kiselev.commands.project;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.entity.Project;
import ru.kiselev.entity.Task;
import ru.kiselev.service.EditorContext;
import ru.kiselev.service.TaskService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import static ru.kiselev.service.ConstantsList.PROJECT_ADD_TASK;


@Data
public class ProjectAddTask implements CmdInterface {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final TaskService taskService;
    private final String name = PROJECT_ADD_TASK;
    private final String description = "Добавить задачу в проект.";

    @Override
    public void execute() throws IOException {
        showTasks(taskService);
        selectTask(taskService);
    }

    private void showTasks(TaskService taskService) {
        System.out.println("\nВыберите задачу которую нужно добавить в проект.");
        List<Task> tsk = taskService.getListTask();
        if (tsk.isEmpty()) {
            System.out.println("Список задач пуст.\n");
            return;
        }
        for (int i = 0; i < tsk.size(); i++) {
            Task task = tsk.get(i);
            if (task.getIdPrj() == 0) {
                String line = String.format("%s. %s", i + 1, task);
                System.out.println(line);
            }
        }
    }

    private void selectTask(TaskService taskService) throws IOException {
        Project project = EditorContext.getCurrentProject();
        System.out.println("Введите id задачи: ");
        long id = Long.parseLong(reader.readLine());
        Task task = taskService.getTask(id);
        project.addNewTask(id);
        task.setIdPrj(project.getId());
        System.out.println("Задача успешно добавлена в проект.\n");
    }
}
