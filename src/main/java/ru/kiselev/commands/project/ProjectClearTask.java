package ru.kiselev.commands.project;

import lombok.Data;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.entity.Project;
import ru.kiselev.exception.NotInputException;
import ru.kiselev.service.EditorContext;
import ru.kiselev.service.TaskService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static ru.kiselev.service.ConstantsList.PROJECT_CLEAR_TASK;


@Data
public class ProjectClearTask implements CmdInterface {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final TaskService taskService;
    private final String name = PROJECT_CLEAR_TASK;
    private final String description = "Удалить задачу из проекта";

    @Override
    public void execute() throws IOException {
        Project project = EditorContext.getCurrentProject();
        System.out.println("Удалить задачу. \nВведите ID задачи: ");
        String read = reader.readLine();
        if (read.isEmpty()) {
            throw new NotInputException();
        }
        long id = Long.parseLong(read);
        taskService.removeTask(id);
        project.getTaskIdList().remove(id);
        System.out.println("Задача " + id + " успешно удалена.\n");
    }
}
