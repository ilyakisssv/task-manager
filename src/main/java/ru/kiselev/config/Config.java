package ru.kiselev.config;

import ru.kiselev.app.anotation.Bean;
import ru.kiselev.app.anotation.Configuration;
import ru.kiselev.app.anotation.Qualifier;
import ru.kiselev.bootstrap.Bootstrap;
import ru.kiselev.commands.CmdInterface;
import ru.kiselev.commands.Help;
import ru.kiselev.commands.project.*;
import ru.kiselev.commands.task.*;
import ru.kiselev.commands.user.*;
import ru.kiselev.repository.ProjectRepository;
import ru.kiselev.repository.TaskRepository;
import ru.kiselev.repository.UserRepository;
import ru.kiselev.service.*;
import ru.kiselev.users.Role;
import ru.kiselev.users.User;
import ru.kiselev.users.UserContext;

import java.util.HashMap;
import java.util.Map;

import static ru.kiselev.service.ConstantsList.*;

@Configuration
public class Config {
    @Bean
    public Bootstrap bootstrap(CommandTerminal commandTerminal) {
        return new Bootstrap(commandTerminal);
    }

    @Bean
    public UserService userService(UserRepository userRepository) {
        UserService userService = new UserService(userRepository);
        User user = new User();
        User admin = new User();

        user.setLogin("user");
        user.setPassword(UserContext.hashPass("11111111"));
        user.setRole(Role.USER);
        userService.createUser(user);

        admin.setLogin("admin");
        admin.setPassword(UserContext.hashPass("000000"));
        admin.setRole(Role.ADMIN);
        userService.createUser(admin);
        return userService;
    }

    @Bean
    public ProjectService projectService(ProjectRepository projectRepository) {
        return new ProjectService(projectRepository);
    }

    @Bean
    public TaskService taskService(TaskRepository taskRepository, ProjectService projectService) {
        return new TaskService(taskRepository, projectService);
    }

    @Bean
    public TaskRepository taskRepository() {
        return new TaskRepository();
    }

    @Bean
    public ProjectRepository projectRepository() {
        return new ProjectRepository();
    }

    @Bean
    public UserRepository userRepository() {
        return new UserRepository();
    }

    @Bean
    public CommandHolder commandConsoleHolder(UserService userService, ProjectService projectService, TaskService taskService,
                                              @Qualifier("commandTerminalProject") CommandTerminal commandTerminalPrj,
                                              @Qualifier("commandTerminalTask") CommandTerminal commandTerminal) {
        HashMap<String, CmdInterface> commands = new HashMap<String, CmdInterface>() {{
            put(HELP, new Help(this));
            put("user-create", new UserCreate(userService));
            put("user-checkin", new UserCheckin(userService));
            put("update-password", new UpdatePassword(userService));
            put("user-view", new UserView(userService));
            put("end-session", new EndOfSession());
            put(PROJECT_CREATE, new CreateProject(projectService));
            put(PROJECT_LIST, new ListProject(projectService));
            put(PROJECT_OPEN, new OpenProject(projectService, taskService, commandTerminalPrj));
            put(PROJECT_REMOVE, new RemoveProject(taskService, projectService));
            put(PROJECT_CLEAR, new ClearProject(projectService));
            put(TASK_CREATE, new CreateTask(taskService));
            put(TASK_LIST, new ListTask(taskService));
            put(TASK_OPEN, new OpenTask(taskService, commandTerminal));
            put(TASK_REMOVE, new RemoveTask(taskService));
        }};
        return new CommandHolder(commands);
    }

    @Bean
    public CommandExecutor commandExecutor(@Qualifier("commandConsoleHolder") CommandHolder commandHolder) {
        return new CommandExecutor(commandHolder);
    }

    @Bean
    public CommandTerminal consoleTerminal(@Qualifier("commandExecutor") CommandExecutor commandExecutor) {
        return new CommandTerminal(commandExecutor);
    }

    @Bean
    public CommandHolder commandHolderProject(TaskService taskService) {
        Map<String, CmdInterface> commandsProject = new HashMap<String, CmdInterface>() {{
            put(PROJECT_NEW_NAME, new NewNameProject());
            put(PROJECT_NEW_DESCRIPTION, new NewDescriptionProject());
            put(PROJECT_ADD_TASK, new ProjectAddTask(taskService));
            put(PROJECT_CLEAR_TASK, new ProjectClearTask(taskService));
            put(TASK_CLEAR, new ClearTask(taskService));
            put("help", new Help(this));
        }};
        return new CommandHolder(commandsProject);
    }

    @Bean
    public CommandExecutor commandExecutorProject(@Qualifier("commandHolderProject") CommandHolder commandHolder) {
        return new CommandExecutor(commandHolder);
    }

    @Bean
    public CommandTerminal commandTerminalProject(@Qualifier("commandExecutorProject") CommandExecutor commandExecutor) {
        return new CommandTerminal(commandExecutor);
    }

    @Bean
    public CommandHolder commandHolderTask() {
        Map<String, CmdInterface> commandsTask = new HashMap<String, CmdInterface>() {{
            put(TASK_NEW_NAME, new NewNameTask());
            put(TASK_NEW_DESCRIPTION, new NewDescriptionTask());
            put("help", new Help(this));
        }};
        return new CommandHolder(commandsTask);
    }

    @Bean
    public CommandExecutor commandExecutorTask(@Qualifier("commandHolderTask") CommandHolder commandHolder) {
        return new CommandExecutor(commandHolder);
    }

    @Bean
    public CommandTerminal commandTerminalTask(@Qualifier("commandExecutorTask") CommandExecutor commandExecutor) {
        return new CommandTerminal(commandExecutor);

    }
}
