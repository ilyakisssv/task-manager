package ru.kiselev.service;

import ru.kiselev.exception.CommandNotFoundException;
import ru.kiselev.exception.NotInputException;

import java.io.IOException;


public class CommandExecutor {
    private final CommandHolder commandHolder;

    public CommandExecutor(CommandHolder commandHolder) {
        this.commandHolder = commandHolder;
    }

    public void execute(String name) throws IOException {
        String normalizedName = normalize(name);
        if (normalizedName.isEmpty()) {
            throw new NotInputException();
        } else {
            try {
                commandHolder.findCommand(name).execute();
            } catch (CommandNotFoundException e) {
                System.out.println("\nОшибка: " + e);
            }
        }
    }

    private String normalize(String name) {
        return name.trim();
    }
}
