package ru.kiselev.service;

import ru.kiselev.entity.Project;
import ru.kiselev.entity.Task;
import ru.kiselev.exception.RightNotFoundException;
import ru.kiselev.exception.TaskNotFoundException;
import ru.kiselev.repository.TaskRepository;
import ru.kiselev.users.UserContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TaskService {
    private final TaskRepository taskRepository;
    private final ProjectService projectService;

    public TaskService(TaskRepository taskRepository, ProjectService projectService) {
        this.taskRepository = taskRepository;
        this.projectService = projectService;
    }

    public Task getTask(Long id) {
        Task task = taskRepository.read(id);
        if (task == null) {
            throw new TaskNotFoundException(id);
        }
        if (task.getIdUser() == UserContext.getIdUser()) {
            return task;
        }
        throw new RightNotFoundException();
    }

    public List<Task> getListTask() {
        List<Task> listTask = new ArrayList<>();
        for (Task task : taskRepository.readAll()) {
            if (task.getIdUser() == UserContext.getIdUser()) {
                listTask.add(task);
            }
        }
        return listTask;
    }

    public void removeTaskList(Set<Long> listId) {
        for (Long id : listId) {
            if (taskRepository.read(id).getIdUser() == UserContext.getIdUser()) {
                taskRepository.remove(id);
            }
        }
    }

    public void removeTask(long id) {
        Task task = taskRepository.read(id);
        if (task == null) {
            throw new TaskNotFoundException(id);
        }
        if (task.getIdUser() != UserContext.getIdUser()) {
            throw new RightNotFoundException();
        }
        long idProject = task.getIdPrj();
        if (idProject != 0) {
            Project project = projectService.getProject(idProject);
            project.getTaskIdList().remove(id);
        }
        taskRepository.remove(id);
    }

    public void createTask(Task task) {
        taskRepository.create(task);
    }
}
