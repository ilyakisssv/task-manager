package ru.kiselev.service;

import ru.kiselev.entity.Project;
import ru.kiselev.exception.ProjectNotFoundException;
import ru.kiselev.exception.RightNotFoundException;
import ru.kiselev.repository.ProjectRepository;
import ru.kiselev.users.UserContext;

import java.util.ArrayList;
import java.util.List;

public class ProjectService {
    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void clearProject() {
        long idUser = UserContext.getIdUser();
        for (Project project : projectRepository.readAll()) {
            if (project.getIdUser() == idUser) {
                projectRepository.remove(project.getId());
            }
        }
    }

    public void createProject(Project project) {
        projectRepository.create(project);
    }

    public Project getProject(long idPrj) {
        Project project = projectRepository.read(idPrj);
        if (project == null) {
            throw new ProjectNotFoundException(idPrj);
        }
        if (project.getIdUser() == UserContext.getIdUser()) {
            return project;
        }
        throw new RightNotFoundException();
    }

    public List<Project> getListProject() {
        List<Project> listProject = new ArrayList<>();
        for (Project project : projectRepository.readAll()) {
            if (project.getIdUser() == UserContext.getIdUser()) {
                listProject.add(project);
            }
        }
        return listProject;
    }

    public void removeProject(long id) {
        Project project = projectRepository.read(id);
        if (project.getIdUser() != UserContext.getIdUser()) {
            throw new RightNotFoundException();
        }
        projectRepository.remove(id);
    }
}
