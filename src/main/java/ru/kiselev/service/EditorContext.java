package ru.kiselev.service;

import ru.kiselev.entity.Project;
import ru.kiselev.entity.Task;

public class EditorContext {
    private static Project currentProject;
    private static Task currentTask;

    public static void clean() {
        currentProject = null;
        currentTask = null;
    }

    public static Project getCurrentProject() {
        if (currentProject == null) {
            throw new IllegalStateException("");
        }
        return currentProject;
    }

    public static void setCurrentProject(Project currentProject) {
        EditorContext.currentProject = currentProject;
    }

    public static Task getCurrentTask() {
        return currentTask;
    }

    public static void setCurrentTask(Task currentTask) {
        EditorContext.currentTask = currentTask;
    }
}
