package ru.kiselev.service;

import ru.kiselev.exception.InvalidUsernameAndPasswordException;
import ru.kiselev.exception.UserNotFoundException;
import ru.kiselev.exception.UserRegisteredException;
import ru.kiselev.repository.UserRepository;
import ru.kiselev.users.User;
import ru.kiselev.users.UserContext;

import java.util.List;

public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUser(long id) {
        return userRepository.read(id);
    }

    public void createUser(User user) {
        userRepository.create(user);
    }


    public void checkingExistingUser(String login) {
        List<User> userList = userRepository.readAll();
        for (User user : userList) {
            if (user.getLogin().equals(login)) {
                throw new UserRegisteredException(login);
            }
        }
    }

    public void checkUser(User user, String password) {
        String login = user.getLogin();
        if (user.getPassword().equals(password)) {
            UserContext.setUser(user);
            System.out.println("Пользователь под логином: " + login + " успешно авторизован.\n");
            return;
        }
        throw new InvalidUsernameAndPasswordException();
    }

    public User checkLoginUser(String login) {
        List<User> userList = userRepository.readAll();
        for (User user : userList) {
            if (user.getLogin().equals(login)) {
                return user;
            }
        }
        throw new UserNotFoundException(login);
    }
}
