package ru.kiselev.service;

import ru.kiselev.commands.CmdInterface;
import ru.kiselev.exception.CommandNotFoundException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;

public class CommandHolder {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final Map<String, CmdInterface> map;

    public CommandHolder(Map<String, CmdInterface> map) {
        this.map = map;
    }

    public CmdInterface findCommand(String name) {
        CmdInterface commandExecute = map.get(name);
        if (commandExecute == null) {
            throw new CommandNotFoundException(name);
        }
        return commandExecute;
    }
}
