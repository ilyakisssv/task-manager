package ru.kiselev.service;

public class ConstantsList {
    public static final String HELP = "help";
    public static final String PROJECT_CREATE = "project-create";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PROJECT_LIST = "project-list";
    public static final String TASK_LIST = "task-list";
    public static final String PROJECT_REMOVE = "project-remove";
    public static final String PROJECT_OPEN = "project-open";
    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_REMOVE = "task-remove";
    public static final String TASK_OPEN = "task-open";
    public static final String TASK_NEW_NAME = "task-new-name";
    public static final String TASK_NEW_DESCRIPTION = "task-new-description";
    public static final String PROJECT_ADD_TASK = "project-add-task";
    public static final String PROJECT_CLEAR_TASK = "project-clear-task";
    public static final String PROJECT_CLEAR_ALL_TASK = "project-clear-all-task";
    public static final String PROJECT_NEW_DESCRIPTION = "project-new-description";
    public static final String PROJECT_NEW_NAME = "project-new-name";
}


