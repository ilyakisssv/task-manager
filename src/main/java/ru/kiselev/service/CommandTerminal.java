package ru.kiselev.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class CommandTerminal {
    private final CommandExecutor commandExecutor;
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public CommandTerminal(CommandExecutor commandExecutor) {
        this.commandExecutor = commandExecutor;
    }

    public void startCommand() {
        System.out.println("\nДля вызова списка команд введите: help.");
        System.out.println("Для завершения работы с задачей введите: exit.");
        while (true) {
            try {
                String inputUser = reader.readLine();
                if (inputUser.equals("exit")) {
                    break;
                }
                commandExecutor.execute(inputUser);
            } catch (Exception e) {
                System.out.println("Произошла ошибка: " + e.getMessage() + " Начните заново.");
            }
        }
    }
}
