package ru.kiselev;


import ru.kiselev.app.ApplicationContext;
import ru.kiselev.bootstrap.Bootstrap;

public class Applications {

    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
        ApplicationContext.construct(Applications.class);
        Bootstrap bean = ApplicationContext.getBean("bootstrap");
        bean.run();
    }
}



