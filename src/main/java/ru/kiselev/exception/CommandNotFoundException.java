package ru.kiselev.exception;

public class CommandNotFoundException extends RuntimeException {
    public CommandNotFoundException(String command) {
        super("Команды c иминем: " + command + " не сушествует.");
    }
}
