package ru.kiselev.exception;

public class ProjectNotFoundException extends RuntimeException {
    public ProjectNotFoundException(long id) {
        super("Проект с id: " + id + " не найден.");
    }
}
