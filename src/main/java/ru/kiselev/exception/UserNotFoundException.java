package ru.kiselev.exception;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(long id) {
        super("Пользователь с id: " + id + " не найден.");
    }

    public UserNotFoundException(String login) {
        super("Пользователь с логином: " + login + " не найден.");
    }

    public UserNotFoundException() {
        super("Пользователь не авторизован.");
    }
}
