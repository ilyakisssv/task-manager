package ru.kiselev.exception;

public class RightNotFoundException extends RuntimeException {
    public RightNotFoundException() {
        super("Отсутствуют права! Данная команда не доступана.");
    }
}
