package ru.kiselev.exception;

public class UserRegisteredException extends RuntimeException {
    public UserRegisteredException(String login) {
        super("Пользователь под логином " + login + " уже существует.\n Выберите другой логин и введите его заново.");
    }
}
