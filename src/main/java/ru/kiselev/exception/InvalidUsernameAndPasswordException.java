package ru.kiselev.exception;

public class InvalidUsernameAndPasswordException extends RuntimeException {
    public InvalidUsernameAndPasswordException() {
        super("Неверный логин или пароль. Попробуйте заново.\n");
    }
}
