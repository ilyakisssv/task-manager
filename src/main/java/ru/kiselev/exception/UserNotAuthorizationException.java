package ru.kiselev.exception;

public class UserNotAuthorizationException extends RuntimeException {
    public UserNotAuthorizationException() {
        super("Пользователь не авторизован. \nВойдите в систему под своим логином.");
    }
}
