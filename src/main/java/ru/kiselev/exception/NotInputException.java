package ru.kiselev.exception;

public class NotInputException extends RuntimeException {
    public NotInputException() {
        super("Отсутствует ввод! Начните заново.\n");
    }
}
