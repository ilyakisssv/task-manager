package ru.kiselev.bootstrap;

import ru.kiselev.service.CommandTerminal;

public class Bootstrap implements Runnable {
    private final CommandTerminal commandTerminal;

    public Bootstrap(CommandTerminal commandTerminal) {
        this.commandTerminal = commandTerminal;
    }

    public void run() {
        System.out.println("*** WELCOME TO TASK MANAGER *** \n");
        commandTerminal.startCommand();
    }
}

