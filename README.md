# Task manager
***

## Требования:

- JRE 8

## Используемый стек:

- Java SE 8
- Maven 3.6

## Обраная связь:

**gmail:** ilyakisssv@gmail.com - Киселев Илья

# Сбора и запуск
***

## Команды для сборки:

В папке с исходниками проекта выполнить команды:

```bash
mvn clean       #очистить проект
mvn install     #пересобрать и установить проект
```

## Команды для запуска:

```bash
java -jar task-manager-1.0-SNAPSHOT.jar
```

